-- MySQL Script generated by MySQL Workbench
-- 09/29/15 13:25:26
-- Model: New Model    Version: 1.0
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema cnh14e
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `cnh14e` ;
CREATE SCHEMA IF NOT EXISTS `cnh14e` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `cnh14e` ;

-- -----------------------------------------------------
-- Table `cnh14e`.`pet_store`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cnh14e`.`pet_store` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cnh14e`.`pet_store` (
  `pst_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(20) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` CHAR(9) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_phone` CHAR(10) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cnh14e`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cnh14e`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cnh14e`.`customer` (
  `cus_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_fname` VARCHAR(20) NOT NULL,
  `cus_phone` CHAR(10) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(20) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` CHAR(9) NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_url` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cnh14e`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cnh14e`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cnh14e`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` TINYINT UNSIGNED NOT NULL,
  `cus_id` TINYINT UNSIGNED NULL,
  `pet_name` VARCHAR(20) NOT NULL,
  `pet_sex` ENUM('m','f') NOT NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM('y','n') NOT NULL,
  `pet_neuter` ENUM('y','n') NOT NULL,
  `pet_notes` VARCHAR(100) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_pet_store_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_pet_store`
    FOREIGN KEY (`pst_id`)
    REFERENCES `cnh14e`.`pet_store` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `cnh14e`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '						';

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `cnh14e`.`pet_store`
-- -----------------------------------------------------
START TRANSACTION;
USE `cnh14e`;
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (1, 'deez pets doe', '228 whoa st', 'tallahassee', 'fl', '32303', 'deezpets.com', 'info@deezpets.com', 21289000.00, '8502662222', 'this store sucks');
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (3, 'lmao pets', '2922 hello rd', 'orlando', 'fl', '32547', 'lmaopetttts.com', 'in@lmaopetttts.com', 22386000.00, '8503851785', 'man screw this place');
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (4, 'starcraft pets', '3030 omg rd', 'atlanta', 'ga', '32304', 'scpets.com', 'pets@scpets.com', 11367000.00, '9415391941', 'nice pets');
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (2, 'tal pets', '928 screwthis st', 'panama city', 'fl', '32308', 'tallahasseepets.com', 'adopt@tallahasseepets.com', 65576000.00, '8505662718', 'has crap breeds');
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (5, 'MO pets', '277 asian avenue', 'destin', 'fl', '32548', 'MOPETSMOLOVE.com', 'mopets@mopetsmolove.com', 89983000.00, '8505666373', 'naw, this place doe');
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (6, 'league pets', '203 bored rd', 'tallahassee', 'fl', '32303', 'leaguepets.com', 'pets@leaguepets.com', 2312313.00, '8502151554', NULL);
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (7, 'awesome pets', '222 fun st', 'sarasota', 'fl', '32032', 'awesomepets.com', 'pets@awesomepets.com', 32323.00, '5151515155', NULL);
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (8, 'cool pets', '800 gogogo rd', 'tampa', 'fl', '23213', 'coolpets.com', 'pets@coolpets.com', 932323.00, '8585656225', NULL);
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (9, 'the pets', '423 finally st', 'jacksonville', 'fl', '23444', 'thepets.com', 'call@thepets.com', 232111.00, '2151562625', NULL);
INSERT INTO `cnh14e`.`pet_store` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_url`, `pst_email`, `pst_ytd_sales`, `pst_phone`, `pst_notes`) VALUES (10, 'lame pets', '332 lamest rd', 'new york', 'ny', '33233', 'lamestoflame.com', 'lamest@lame.com', 456433.00, '5151859262', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cnh14e`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `cnh14e`;
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'nguyen', 'nancy', '8502222222', '3250 woodward', 'tallahassee', 'fl', '32547', 'iamnancy@gmail.com', 'iamnancy.com', 600.00, 266.00, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'cuong', 'king', '3369994785', '2929 hahaha rd', 'panama city', 'fl', '32304', 'iamcuong@email.com', 'kingcuong.com', 500.00, 1000.00, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'yayaya', 'woot', '8503152082', '889 omglol rd', 'destin', 'fl', '32308', 'iamomg@email.com', 'omg.com', 283.00, 523.00, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'guys', 'asian', '8502699941', '223 farts rd', 'sarasota', 'fl', '32548', 'iamwho@email.com', 'lol.com', 99.25, 600.00, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'huynh', 'hien', '8503554946', '222 ohsnaplol rd', 'tallahassee', 'fl', '32305', 'iamwhat@email.com', 'wtfroar.com', 664.21, 524.25, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'girls', 'asian', '8822525252', '938 dangit st', 'panama city', 'fl', '15115', 'floot@woot.com', 'triggin.com', 2323.32, 152.00, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'wow', 'cool', '1545252656', '223 whoaman rd', 'orlando', 'fl', '88582', 'word@what.com', 'fliggin.com', 5515.23, 444532.00, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'farts', 'monkey', '6262626252', '447 thatroad rd', 'new york', 'ny', '55236', 'yupuh@huh.com', 'miggin.com', 856.00, 12512.00, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'lameski', 'thatius', '2626551515', '323 borington st', 'pheonix', 'az', '47854', 'reallynow@what.com', 'kiggin.com', 1254.00, 25266.00, NULL);
INSERT INTO `cnh14e`.`customer` (`cus_id`, `cus_lname`, `cus_fname`, `cus_phone`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_email`, `cus_url`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'what', 'rofloio', '5995959592', '994 lemon rd', 'dallas', 'tx', '96585', 'yeah@google.com', 'riggin.com', 5236.00, 2142.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `cnh14e`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `cnh14e`;
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 1, 1, 'lulu', 'f', 'dog', 5000.00, 5000.00, 2, 'gold', '2014-09-20', 'y', 'y', 'great friend');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 2, NULL, 'sugar', 'f', 'iguana', 10.00, 8000.00, 3, 'white', '2014-09-13', 'y', 'y', 'loyal');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 3, 3, 'kito', 'm', 'dog', 1000000.00, 3000.00, 2, 'multi', '2014-07-16', 'n', 'n', 'protector');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 4, 4, 'chrono', 'm', 'dog', 600000.00, 5000.00, 3, 'brown', '2014-03-18', 'n', 'n', 'knows tricks');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 5, NULL, 'cookie', 'm', 'dog', 302.00, 7000.00, 4, 'black', '2014-05-12', 'y', 'y', 'potty trained');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 6, 6, 'toby', 'm', 'dog', 100.00, 2320.00, 1, 'brown', '2014-06-01', 'n', 'n', 'na');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 7, 7, 'doggie', 'f', 'dog', 150.00, 2523.00, 2, 'black', '2014-08-08', 'n', 'n', 'na');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 8, 8, 'kitty', 'f', 'cat', 120.00, 12521.00, 3, 'brown', '2014-02-22', 'n', 'n', 'na');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 9, 9, 'fishy', 'm', 'fish', 984.00, 2363.00, 3, 'gold', '2014-02-15', 'n', 'n', 'na');
INSERT INTO `cnh14e`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_name`, `pet_sex`, `pet_type`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (NULL, 10, 10, 'sharky', 'm', 'fish', 15251.00, 5265.00, 8, 'white', '2014-03-09', 'n', 'n', 'na');

COMMIT;

